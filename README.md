# urbandict
urbandict is a small Python script that allows you to search the awesome urbandictionary.com database and print the results directly to the console without the need to use a browser.

To get everything working you need the following Python packages installed: `sys`, `os`, `json`, `urllib`, `argparse`, `httplib2`, `colorama`. Please check if you have installed them before using the program to avoid errors.
To install `colorama` have a look [here](https://pypi.python.org/pypi/colorama).

## Usage
To search the database for the urban definition of "world peace" use 

	python urbandict.py -d "world peace"
	
This will show you that it is impossible by definition to have world peace.

You can also show more details which include examples and information about the author, entry id and up-/down votes. To do so use 

	python urbandict.py -d "world peace" --details

## Remarks
Urban Dictionary® is a registered trademark. You find them on [urbandictionary.com](http://www.urbandictionary.com/).

Please note, that this is a private project. Urban Dictionary® is not associated in any way with this project.
